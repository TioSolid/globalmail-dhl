import urllib
import re
import argparse
import sys
import time

#Change to suit updates to mobile site
dpExpression = '<dt>Delivery[\s\S]*?<dd>(.*)<\/dd>'

#Change to suit your custom tracking list for whatever reason
tbExpression = '(GM[0-9]{18})'

#Query a tracking code, returns the Deliver Partner code if found, empty string if not
def queryTrack(trackingNumber):
	params = urllib.urlencode(dict(trackingnumber=trackingNumber))
	output = urllib.urlopen("http://webtrack.dhlglobalmail.com/?" + params)

	match = re.search(dpExpression, output.read(), re.M)

	if match: 
		return match.group(1)
	else: 
		return False

def parseTracking(trackinLine):
	match = re.search(tbExpression, trackinLine)

	if match:
		return match.group(1)
	else:
		return False


#TODO: Args explanation
parser = argparse.ArgumentParser(description='Reads a file with DHL GlobalMail tracking numbers and gets all avaliable Post Office Tracking Codes',
								epilog='The resulting output will be written to the output.txt file')
parser.add_argument('-list', metavar='tracklist', type=str, default="track.txt", nargs="?",
                   help='Tracking list (defaults to track.txt)')

try:
	#Parse the prompt arguments
	args = parser.parse_args()
	listHandle = open(args.list)
except Exception, e:
	parser.print_help()
	print "\nError - %s" %e
	sys.exit(1)

gmCounter = 0
trackCounter = 0

try:
	#create an output file
	outputHandle = open('output.txt', 'w')
	#Read the tracking list and query the Codes
	for line in listHandle:
		trackingNumber = parseTracking(line)
		if trackingNumber:
			gmCounter += 1
			outputHandle.write('Tracking: %s\n' %trackingNumber)
			print "Querying the %s number.. " %trackingNumber,
			partnerCode = queryTrack(trackingNumber)
			outputHandle.write('\tDelivery Code: ')
			if partnerCode:
				print "Delivery code FOUND!"
				trackCounter += 1
				outputHandle.write(partnerCode + '\n')
			else:
				print "Delivery code NOT FOUND :("
				outputHandle.write('NOT FOUND\n')

		time.sleep(1) #waits one second before next query
except Exception, e:
	print "Error while processing the track list - %s" %e
	sys.exit(1)

print "Finished! %s GM Code(s) Processed, %s tracking code(s) found" %(gmCounter, trackCounter)